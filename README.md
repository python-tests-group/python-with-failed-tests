### Description
The simple class that implements calculator functions for two operands, with a testing suite. 

### Tests
```bash
# Install Pytest
pip install -r requirements.txt

# Run tests
pytest
```
