import pytest

from calculator import Calculator


@pytest.mark.skip(reason="no way of currently testing this")
def test_mul():
    calc = Calculator(5, 7)
    assert calc.mul() == 5 * 7


def test_div():
    calc = Calculator(5, 7)
    assert calc.div() == 5 / 0
